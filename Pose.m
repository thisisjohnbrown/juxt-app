//
//  Pose.m
//  Juxt
//
//  Created by John Brown on 8/2/13.
//  Copyright (c) 2013 John Brown. All rights reserved.
//

#import "Pose.h"


@implementation Pose

@dynamic direction;
@dynamic beforePath;
@dynamic afterPath;
@dynamic perc;
@dynamic name;

@end
