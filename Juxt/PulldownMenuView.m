//
//  PulldownMenuView.m
//  Juxt
//
//  Created by John Brown on 8/5/13.
//  Copyright (c) 2013 John Brown. All rights reserved.
//

#import "PulldownMenuView.h"

@implementation PulldownMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    self.backgroundColor = [UIColor grayColor];
}

@end
