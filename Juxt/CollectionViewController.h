//
//  CollectionViewController.h
//  Juxt
//
//  Created by John Brown on 8/5/13.
//  Copyright (c) 2013 John Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UIViewController
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@end
