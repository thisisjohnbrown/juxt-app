//
//  Pose.m
//  Juxt
//
//  Created by John Brown on 8/11/13.
//  Copyright (c) 2013 John Brown. All rights reserved.
//

#import "Pose.h"


@implementation Pose

@dynamic afterOffsetX;
@dynamic afterOffsetY;
@dynamic afterPath;
@dynamic afterScale;
@dynamic beforeOffsetX;
@dynamic beforeOffsetY;
@dynamic beforePath;
@dynamic beforeScale;
@dynamic caption;
@dynamic createdDate;
@dynamic direction;
@dynamic identifier;
@dynamic perc;
@dynamic ratio;
@dynamic updatedDate;
@dynamic uploaded;
@dynamic uri;
@dynamic name;
@dynamic username;
@dynamic shared;
@dynamic draft;

@end
